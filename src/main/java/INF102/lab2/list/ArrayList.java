package INF102.lab2.list;

import java.security.interfaces.ECPublicKey;
import java.util.Arrays;
import java.util.Iterator;

import javax.swing.text.AbstractDocument.ElementEdit;

public class ArrayList<T> implements List<T> {

	
	public static final int DEFAULT_CAPACITY = 10;
	
	private int n;
	
	private Object elements[];
	
	public ArrayList() {
		elements = new Object[DEFAULT_CAPACITY];
	}
		
	@Override
	public T get(int index) {
		if(isEmpty()){
			throw new IndexOutOfBoundsException();
		}
		return (T) elements[index];
	}
	
	@Override
	public void add(int index, T element) {
		if(index>n){
			throw new IndexOutOfBoundsException();
		}
		n++;
		//double array if no more space
		if(elements.length < size()){
			Object[] newArray = new Object[elements.length + DEFAULT_CAPACITY];
			for (int i = 0; i < size(); i++) {
				if(index == i){
					newArray[i] = element;
				}
				else if( i < index){
					newArray[i] = elements[i];
				}
				else{
					newArray[i] = elements[i-1];
				}
				
			}
			elements = newArray;
		}

		else{
			for (int i = size()-1; i > index; i--) {
				elements[i]=elements[i-1];
			}
			elements[index] = element;

		}
		
	}
	
	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n*3 + 2);
		str.append("[");
		for (int i = 0; i < n; i++) {
			str.append((T) elements[i]);
			if (i != n-1)
				str.append(", ");
		}
		str.append("]");
		return str.toString();
	}

}